﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HighscoreKeeperService.Models
{
    public class Highscore
    {
        public int HighscoreId { get; set; }
        public string PlayerName { get; set; }
        public int GameId { get; set; }
        public long Score { get; set; }
    }
}
