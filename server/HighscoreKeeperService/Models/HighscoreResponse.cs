﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HighscoreKeeperService.Models
{
    public class HighscoreResponse
    {
        public List<Highscore> Scores { get; set; }
    }
}
