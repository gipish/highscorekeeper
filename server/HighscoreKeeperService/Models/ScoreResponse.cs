﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HighscoreKeeperService.Models
{
    public class ScoreResponse
    {
        public int GameId { get; set; }

        public string PlayerName { get; set; }

        public int Score { get; set; }

        public ScoreResponse(int gameId, string player, int score)
        {
            GameId = gameId;
            PlayerName = player;
            Score = score;
        }
    }
}
