﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HighscoreKeeperService.Database;
using HighscoreKeeperService.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HighscoreKeeperService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GamesController : ControllerBase
    {
        private readonly GamesStore _gamesStore;
        public GamesController(GamesStore gamesStore)
        {
            _gamesStore = gamesStore;
        }
        // GET: api/Games
        [HttpGet]
        public List<Game> Get()
        {
            return _gamesStore.GetAllGames().Result;
        }

        // GET: api/Games/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Games
        [HttpPost]
        public IActionResult AddGame([FromBody] CreateGameRequest request)
        {
            if(request.Name.Length > 0)
            {
                _gamesStore.AddGame(request.Name, request.ImageUrl);
                return Created("AddGame", request.Name);
            }

            return new StatusCodeResult(500);
        }

        // PUT: api/Games/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
