﻿using HighscoreKeeperService.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HighscoreKeeperService.Database
{
    public class GamesContext : DbContext
    {
        public DbSet<Game> Games { get; set; }
        public DbSet<Highscore> Highscores { get; set; }

        public GamesContext(DbContextOptions<GamesContext> options) :base(options)
        {

        }
    }
}
