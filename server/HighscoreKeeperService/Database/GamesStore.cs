﻿using HighscoreKeeperService.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HighscoreKeeperService.Database
{
    public class GamesStore
    {
        private readonly GamesContext _gamesContext;
        private readonly ILogger _logger;
        public GamesStore(GamesContext gamesContext, ILogger<GamesStore> logger)
        {
            _gamesContext = gamesContext;
            _logger = logger;
        }

        public async Task AddGame(string name, string imageUrl)
        {
            await _gamesContext.Games.AddAsync(new Game
            {
                Name = name,
                ImageUrl = imageUrl
            });

            await _gamesContext.SaveChangesAsync();
        }

        public async Task<bool> AddScore(int gameId, string player, int score)
        {
            try
            {
                await _gamesContext.Highscores.AddAsync(new Highscore
                {
                    GameId = gameId,
                    PlayerName = player,
                    Score = score
                });

                await _gamesContext.SaveChangesAsync();
                return true;
            } catch (Exception ex)
            {
                _logger.LogError("Error while adding score!", ex.Message);
                return false;
            }

        }

        public async Task<List<Highscore>> GetHighscoresByGame(int gameId)
        {
            try
            {
                var scores = _gamesContext.Highscores.Select(hs => hs).Where(hs => hs.GameId == gameId).ToList();
                return scores;
            } 
            catch (Exception ex)
            {
                _logger.LogError("Error while getting highscores", ex.Message);
                return null;
            }
        }

        public async Task<List<Game>> GetAllGames()
        {
            return _gamesContext.Games.Select(g => g).ToList();
        }
    }
}
