﻿using HighscoreKeeperService.Database;
using HighscoreKeeperService.Models;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HighscoreKeeperService.Hubs
{
    public class ScoreHub : Hub
    {
        private readonly GamesStore _gamesStore;
        public ScoreHub(GamesStore gamesStore)
        {
            _gamesStore = gamesStore;
        }
        public async Task SendMessage(string user, string message)
        {
            await Clients.All.SendAsync("ReceiveMessage", user, message);
        }

        public async Task SaveScore(int gameId, string player, int score)
        {
            var success = await _gamesStore.AddScore(gameId, player, score);
            if(success)
            {
                var response = new ScoreResponse(gameId, player, score);
                await Clients.All.SendAsync("ScoreSaved", response);
            }
            else
            {
                await Clients.Caller.SendAsync("Error", "There was an error saving the score. Try again!");
            }
        }

        public async Task GetGames()
        {
            var games = await _gamesStore.GetAllGames();
            await Clients.Caller.SendAsync("ReceivedGames", games);
        }

        public async Task GetScores(int gameId)
        {
            var scores = await _gamesStore.GetHighscoresByGame(gameId);

            await Clients.Caller.SendAsync("ReceivedScores", scores);
        }
    }
}
