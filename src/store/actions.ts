import { ActionTree } from 'vuex';
import { IState } from './state';

const actions: ActionTree<IState, IState> = {
    getGames({ commit }, payload): any {
        // get games.
        console.log(payload);
        commit('loadGames', payload);
    },
    loadScores({ commit }, payload): any {
        console.log(payload);
        commit('loadedScores', payload);
    },
    addScore({ commit }, payload): any {
        console.log(payload);
        commit('addedScore', payload);
    },
    selectGame({ commit }, payload): any {
        console.log(payload);
        commit('selectedGame', payload);
    },
};

export default actions;
