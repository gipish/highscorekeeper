import { MutationTree } from 'vuex';
import { IState, IHighscore } from './state';

const mutations: MutationTree<IState> = {
    addedScore(state, payload) {
        // add score to game?
        var highscores = state.highscoreList;
        var index = highscores.findIndex(list => list.key == payload.gameId);
        if(index != -1)
        {
            highscores[index].highscores.push(payload);
        }
        else
        {
            var newScores = [];
            newScores.push(payload);
            highscores.push({key: payload.gameId, highscores: newScores})
        }

        state.highscoreList = highscores;
    },
    loadGames(state, payload) {
        state.games = payload;
    },
    selectedGame(state, payload) {
        state.gameSelected = payload;
    },
    loadedScores(state, payload) {
        console.log("mutation");
        console.log(payload);
        var highscores = state.highscoreList;
        var gameId = payload[0].gameId;
        if(payload.length > 0)
        {
            if(highscores)
            {
                var index = -1;
                index = highscores.findIndex(list => list.key == gameId);
                if(index != -1)
                {
                    highscores[index] = payload;
                }
                else
                {
                    highscores.push({key: gameId, highscores: payload});
                }
            }
        }

        state.highscoreList = highscores;
    }
};

export default mutations;
