import { IState, IGame, IHighscoreList } from './state';
import { GetterTree } from 'vuex';

const getters: GetterTree<IState, IState> = {
    games(state: IState): IGame[] { return state.games; },
    highscores(state: IState): IHighscoreList[] { return state.highscoreList; },
    gameSelected(state: IState): number { return state.gameSelected }
};

export default getters;
