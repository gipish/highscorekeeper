export interface IGame {
    imageUrl: string;
    id: number;
    name: string;
}

export interface IHighscore {
    gameId: number;
    playerName: string;
    score: number;
    highscoreId: number;
}

export interface IHighscoreList {
    key: string;
    highscores: IHighscore[];
}

export interface IState {
    games: IGame[];
    gameSelected: number;
    highscoreList: IHighscoreList[];
}

const defaultState = {
    games: [
        {imageUrl: '', id: 1, name: 'Pac-man'},
    ],
    highscoreList: [

    ],
    gameSelected: -1
};

export default defaultState;
