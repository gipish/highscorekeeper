
import * as signalR from '@microsoft/signalr';
import * as config from './config.json';

const connection = new signalR.HubConnectionBuilder()
  .withUrl(config.signalRUrl + "/scorehub")
  .build();

let subscriptions = [];

async function start() {
  try {
    await connection.start();
    console.log('connected');
    return Promise.resolve(connection);
  } catch (err) {
    console.log('err', err);
    console.log('reconnecting')
    return Promise.reject("Connecting to server failed")
  }
}

async function saveScore(gameId: number, playerName: string, score: number)
{
  try {
    console.log(connection);
    await connection.invoke('SaveScore', gameId, playerName, score);
  } catch (err) {
    console.log('err', err);
  }
}

function getScores(gameId: number)
{
  try {
    connection.invoke('GetScores', gameId);
  } catch (err) {
    console.log('err', err);
  }
}

export {
  start,
  getScores,
  saveScore
};
